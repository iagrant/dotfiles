#!/bin/bash

#copy rcs
cp ~/.vimrc rcs/vimrc
cp ~/.zshrc rcs/zshrc
cp ~/.bashrc rcs/bashrc
cp ~/.config/kitty/kitty.conf rcs/kitty.conf
cp ~/.i3/config rcs/i3config
