#!/bin/bash

sudo apt install -y pandoc
sudo apt install -y texlive texlive-latex-base texlive-latex-extra texlive-fonts-extra
git clone https://github.com/Wandmalfarbe/pandoc-latex-template.git eisvogel
sudo cp eisvogel/eisvogel.tex /usr/share/pandoc/data/templates/
rm -rf eisvogel
mkdir -p $HOME/.local/bin
cp ../scripts/gen-pdf $HOME/.local/bin/gen-pdf
chmod +x $HOME/.local/bin/gen-pdf
