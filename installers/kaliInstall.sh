#!/bin/bash

echo "Giving user access to /opt"
sudo chown -R $USER /opt
echo "Installing python3 pip3 python3-venv"
sudo apt install python3 python3-pip python3-venv
echo "Installing python3-bloodhound"
sudo apt install bloodhound
echo "Installing impacket"
git clone https://github.com/SecureAuthCorp/impacket /opt/impacket
cd /opt/impacket
pip3 install .
cd -
"Installing PEASS - Privilege Escalation Awesome Scripts SUITE"
git clone https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite.git /opt/peass
echo "Installing Nishang"
git clone https://github.com/samratashok/nishang /opt/nishang
echo "Installing evil Winrm"
git clone https://github.com/Hackplayers/evil-winrm /opt/evil-winrm
echo "Installing neo4j"
sudo apt install neo4j
echo "Installing BloodHound Git"
git clone https://github.com/BloodHoundAD/BloodHound /opt/BloodHound-git


echo "Download latest version of Ghidra - https://ghidra-sre.org/"
echo "Download latest version of BloodHound srv - https://github.com/BloodHoundAD/BloodHound/releases"
