echo Installing Custom NeoVim and Plugins
./vimInstall.sh
echo Installing Custom Zsh and Plugins
./zshInstall.sh
echo Installing Powerline Fonts
git clone https://github.com/powerline/fonts.git
cd fonts
./install.sh
cd ..
rm -rf fonts

echo Copying Over General Confs
echo --------------------------
cp ../rcs/tmux.conf ~/.tmux.conf
#cp ../rcs/profile ~/.profile
#cp ../rcs/Xmodmap ~/.Xmodmap
#cp ../rcs/xinitrc ~/.xinitrc
#cp ../rcs/Xresources ~/.Xresources
#cp ../rcs/sbclrc ~/.sbclrc
#echo Copying Over Pacman Conf
#cp ../rcs/pacman.conf /etc/pacman.conf
#echo Copying Over i3 Config
#cp ../rcs/i3config ~/.i3/config
#echo Coppying Over Kitty Terminal Confs
#cp ../rcs/kitty.conf ~/.config/kitty/
#cp ../rcs/kittyNord.conf ~/.config/kitty/

echo All Done! Enjoy! :D
